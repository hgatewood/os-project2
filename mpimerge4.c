#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#define MASTER 0

void setUp(int argc, char* argv[], int id, int numProcesses, int* arraySize);
int* merge(int firstArray[], int secondArray[], int mergeResult[]);
int* mSortAlg(int numLevels, int id, int localArray[], MPI_Comm comm, int globalArray[]);
int compare(const void* num1, const void* num2);


char pi_id[MPI_MAX_PROCESSOR_NAME];
int numProcesses, id, validNumProcesses, globalArraySize, localArraySize, numLevels, i, x;
int *localArray, *globalArray, *arraySize;
int size = 0;
double startTime, localTime, totalTime, initialStartTime, initialTotalTime, processStartTime, processTotalTime;


int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numProcesses);
    MPI_Comm_rank(MPI_COMM_WORLD, &id);

    MPI_Get_processor_name(pi_id, &x);

    /*
     Number of processes must be a power of 2 for the program to continue.
     */
    validNumProcesses = (numProcesses != 0) && ((numProcesses & (numProcesses - 1)) == 0);
    if (!validNumProcesses) {
        if (id == MASTER) printf("ERROR: The number of processes must be a power of 2. Please try again. \n");
        MPI_Finalize();
        exit(-1);
    }
    
    // Get's user input regarding desired size of array and the number of processes.
    setUp(argc, argv, id, numProcesses, &globalArraySize);
 
    // Fill the array with random values between 1 and 10
    if(id == MASTER){
        globalArray = (int*) malloc (globalArraySize * sizeof(int));
        srand(id + time(0));
        printf("Original Array (unsorted): \n");
        for (i = 0; i < globalArraySize; i++) {
            globalArray[i] = rand() % 10;
            printf(" %d", globalArray[i]);
        }
        printf("\n");
    }
  
    /*
     Local Array's are the individaul arrays sent to each one of the raspberry pi nodes for sorting.
     */
    localArraySize = globalArraySize / numProcesses;
    size = localArraySize;
    localArray = (int*) malloc (localArraySize * sizeof(int));
    
    MPI_Scatter(globalArray, localArraySize, MPI_INT, localArray, localArraySize, MPI_INT, 0, MPI_COMM_WORLD);
    
    printf("Local Array (unsorted) for Process %d: \n ", id);
    for (i = 0; i < localArraySize; i++) {
        printf(" %d", localArray[i]);
    }
    printf("\n");
    numLevels = log2(numProcesses);

    // Start timing
    startTime = MPI_Wtime();
    
    // Merge Sorting Process
    if(id == MASTER) {
		initialStartTime = MPI_Wtime();
		globalArray = mSortAlg(numLevels, id, localArray, MPI_COMM_WORLD, globalArray);
		initialTotalTime = MPI_Wtime() - initialStartTime;
		printf("Process #%d of %d on %s took %f seconds \n", id, numProcesses, pi_id, initialTotalTime);
	} else {
		processStartTime = MPI_Wtime();
        	mSortAlg(numLevels, id, localArray, MPI_COMM_WORLD, NULL);
		processTotalTime = MPI_Wtime() - processStartTime;
		printf("Process #%d of %d on %s took %f seconds \n", id, numProcesses, pi_id, processTotalTime);
	}
    // End timing
    localTime = MPI_Wtime() - startTime;
    MPI_Reduce(&localTime, &totalTime, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

    if(id == MASTER) {
        printf("Final Array (sorted): \n");
        for(i = 0; i < globalArraySize; i++) {
            printf(" %d", globalArray[i]);
        }
        printf("\n");
		printf("Sorting %d integers took %f seconds .\n", globalArraySize, totalTime);
		free(globalArray);
	}

    free(localArray);
    MPI_Finalize();
    return 0;
}

/*
 Gets input from user for array size.
 */
void setUp(int argc, char* argv[], int id, int numProcesses, int* arraySize){
    if (id == MASTER){
        if ((atoi(argv[1])) % numProcesses != 0) {
            fprintf(stderr, "ERROR: The size of the array must be divisible by number of processes. Please try again. \n");
            *arraySize = -1;
        } else {
            *arraySize = atoi(argv[1]);
        }
    }
    MPI_Bcast(arraySize, 1, MPI_INT, 0, MPI_COMM_WORLD);
}

/*
 Merges local arrays together into mergeResult
 */
int* merge(int firstArray[], int secondArray[], int finalArray[]){
    int a1 = 0;
    int a2 = 0;
    int final = 0;
    
    while ((a1 < size) && (a2 < size)){
        if (firstArray[a1] <= secondArray[a2]) {
            finalArray[final] = firstArray[a1];
            a1++;
        }else {
            finalArray[final] = secondArray[a2];
            a2++;
        }
        final++;
    }
    if(a1 >= size) {
        while (a2 < size) {
            finalArray[final] = secondArray[a2];
            a2++;
            final++;
        }
    }
    if(a2 >= size) {
        while (a1 < size) {
            finalArray[final] = firstArray[a1];
            a1++;
            final++;
        }
    }
    return finalArray;
}

/*
 Merge Sorting Algorithm
 */
int* mSortAlg(int numLevels, int id, int localArray[], MPI_Comm comm, int globalArray[]){
    int l, r, currentLevel;
    int *firstArray, *secondArray, *finalArray;
    
    currentLevel = 0;
    firstArray = localArray;
    
    qsort(localArray, size, sizeof(int), compare); //sorts array
    
    while (currentLevel < numLevels) {
        l = (id & (~(1 << currentLevel)));
        
        if (l == id) {
            r = (id | (1 << currentLevel));
            secondArray = (int*) malloc (size * sizeof(int));
            MPI_Recv(secondArray, size, MPI_INT, r, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            finalArray = (int*) malloc (size * 2 * sizeof(int));
            firstArray = merge(firstArray, secondArray, finalArray);
            size = size * 2;
            currentLevel++;
            
        } else { 
            MPI_Send(firstArray, size, MPI_INT, l, 0, MPI_COMM_WORLD);
            currentLevel = numLevels;
        }
    }
    
    if(id == MASTER){
        globalArray = firstArray;
    }
    return globalArray;
}

/*
 Compares the size of the local array to the size of the orignial array.
 */
int compare(const void* num1, const void* num2) {
    int n1 = *((int*)num1);
    int n2 = *((int*)num2);
    if(n1 == n2) {
        return 0;
    }else if(n1 > n2) {
        return 1;
    }else {
        return -1;
    }
}


